@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains('user_id', Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
                    @csrf
                    <!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
						Post Comment
					</button>
					
					<!-- Modal -->
					<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
						<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Post Comment</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<label for="content">Content:</label>
								<textarea class="form-control" id="postContent" name="postContent" rows="3"></textarea>
							</div>
							<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Post Comment</button>
							</div>
						</div>
						</div>
					</div>
                </form>
            @endif
            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>
	<h2 class="mt-3">Comments:</h2>
	@foreach ($post->comments as $comment)
	<div class="card">
        <div class="card-body">
            <p class="card-subtitle text-muted text-center">{{$comment->content}}</p>
            <p class="card-subtitle text-muted text-end">posted by: {{$comment->user->name}}</p>
            <p class="card-subtitle text-muted text-end">posted on: {{$post->created_at}}</p>
        </div>
    </div>
	@endforeach
@endsection