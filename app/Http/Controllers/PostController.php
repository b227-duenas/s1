<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated userr via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;


class PostController extends Controller
{
    // action to return a view contaning a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    public function store(Request $request){
        if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts.
    public function index(){
        $posts = Post::all()->where('isActive', true);
        //The "with()" method will allow us to pass information from the controller to view page.
        return view('posts.index')->with('posts', $posts);
    }

    //Activity for s02
    //action that will return a view showing random blog posts
    public function welcome()
    {
        // $posts = Post::all();
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->get();
        return view('welcome')->with('posts', $posts);
    }

    //action for showing only the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        } else {
            return redirect('/login');
        }        
    }

    public function update(Request $request, $id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        } 
        return redirect('/posts');
    }

    public function archive($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    public function unarchive($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
            $post->save();
        }
        return redirect('/posts');
    }
    
    public function likes($id){
        $post = Post::find($id);
        if(Auth::user()){
            $user_id = Auth::user()->id;
            if($post->user_id != $user_id){
                if($post->likes->contains('user_id', $user_id)){
                    Postlike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                } else {
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();
                }
                return redirect("/posts/$id");
            }

        } else {
            return redirect('/login');
        }
    }

    public function comment(Request $request, $id){
        $post = Post::find($id);
        $comments = PostComment::find($id);
        if(Auth::user()){
            $user_id = Auth::user()->id;
            if($post->user_id != $user_id){
                if($post->comments->contains('user_id', $user_id)){
                    PostComment::where('post_id', $post->id)->where('user_id', $user_id);
                } else {
                    $postComment = new PostComment;
                    $postComment->post_id = $post->id;
                    $postComment->user_id = $user_id;
                    $postComment->content = $request->input('postContent');
                    $postComment->save();
                }
                return redirect("/posts/$id");
            }

        } else {
            return redirect('/login');
        }
    }
}