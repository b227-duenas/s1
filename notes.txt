[SECTION] Create a laravel project

Syntax:	
	
	composer create-project laravel/laravel <project-name>

After installation we can test if the application is running with the command:

	php artisan serve

[SECTION] Folder Structure

app folder
	- contains the models at its root. Models represent our database entities and have pre-defined methods for querying the respective tables that they represent.

	- "Http" -> Controllers subdirectory contains the project's controllers where we define our application logic.

database folder
	- migrations subdirectory contains the migrations that we will use to define the structures and data types of our database tables.

public folder
	- views subdirectory is the namespace where all views will be looked for by our application.

resources folder
	- where assets such as css, js, images, etc. can be stored and accessed.

routes folder
	- web.php file is where we define the routes of our web application

.env file at the root of our project directory is where we set our application settings including database connection settings


[SECTION] Generate the authentication scafolding (Login & Registration Page)

- install Laravel's laravel/ui package via the terminal command:

	composer require laravel/ui

- then build the authentication scaffolding via the terminal command (This wil create the login & register page):
	
	php artisan ui bootstrap --auth

- compile our fresh scaffolding with the command:
	
	npm install && npm run dev

[SECTION] Generating Model Classes with its corresponding migrations and controllers

	Syntax:

		php artisan make:model <ModelName> -mc

		- Model created is Eloquent Model(Eloquent is Laravel's ORM - Object Relational Model)
			- This will be used by laravel to map its relational database table.
			- Eloquent models have pre-defined methods database queries and operations.

		- Migrations (m) are like version control for your database, allowing your team to define and share the application's database schema definition..
			- A migration class contains two methods:
				- "up" method is used to add new tables, columns, or indexes to your database
				- "down" method should reverse the operations performed by the up method.

		- controller (c) are meant to group associated request handling logic within a single class.


[SECTION] Flow of Routes, Controller, Views

	- Routes (routes folder->web.php)
	- Controller (app folder -> Http folders -> Controllers folders -> controller_name.php)
	- Views (views folder)

[SECTION] Action handled by Resource Controllers

https://laravel.com/docs/9.x/controllers#actions-handled-by-resource-controller

[SECTION] URL Parameters and Selective Querying

"{}" - Route parameters are always encased within a curly braces and should consist of alphabetic characters. 
	- Route parameters are injected into route callbacks / controllers based on their order

For laravel blade we used {{}} which is used to echo data after checking for existence
	- This is a shortcut to check if a specific variable has been set instead using a ternary operator.